import React, { useEffect, useState, Suspense } from 'react';
import  axios from 'axios';
import styled from '@emotion/styled'

import suspensify from '../lib/suspensify';
import { Product } from './Product';

const BundlesPanel = styled.div(`
  width: 100%;
  background-color: #eff6f6;
  padding: 32px;
  border-radius: 32px;
  font-family: helvetica, arial, sans-serif
`)

const Button = styled.button(`
  background-color: #93bc34;
  border: 2px solid #93bc34;
  border-radius: 10px;
  padding: 16px;
  color: #fff; 
  
  &:focus, 
  &:hover {
    background-color: #abd24f;
    border-color: #abd24f;
  }
`)

const PanelHeading = styled.h2(`
  font-size: 2rem;
`)

const ProductList = styled.div({
  display: 'flex'
})

function fetchData() {
  return axios('https://hb.demo/products');
}

const Loading = () => {
  return (<p>LOADING...</p>)
}

const Bundle = ({promise}) => {
  const { products } = promise.read();
  
  const availableProducts = products.filter((product) => {
    return product.stock > 0;
  });

  const initialSelectedItems = availableProducts.map(product => product.sku);

  const [selectedItems, setSelectedItems] = useState(initialSelectedItems);

  const handleCheckboxUpdate = (sku) => {
    return ((event) => {
      if (event.target.checked) {
        setSelectedItems(selectedItems.concat([sku]))
      } else {
        setSelectedItems(selectedItems.filter(currentSku => sku !== currentSku ))
      }
    });
  }

  return (
    <BundlesPanel>

      <PanelHeading>
        Frequently bought together
      </PanelHeading>
      <p>Please ensure you read the label of all products pruchased as part of this bundle before use.</p>

      <div>
        Total Price: £{selectedItems.reduce((total, itemSku) => {
          const selectedProduct = availableProducts.find((product) => {
            return product.sku === itemSku;
          });

          total += parseFloat(selectedProduct.price);
          return total;
        }, 0)}
      </div>

      <Button onClick={() => console.log(selectedItems)}>
        Add {selectedItems.length} items to basket
      </Button>
      
      <ProductList>
      {availableProducts.map((product) => (
        <Product
          product={product} 
          handleCheckboxUpdate={handleCheckboxUpdate(product.sku)} 
          isSelected={selectedItems.includes(product.sku)}
          key={product.sku}
        />
      ))}
      </ProductList>
    </BundlesPanel> 
  )
}

export const Bundles = () => {
  const promise = suspensify(fetchData());
  
  return (
    <div>
      <Suspense fallback={<Loading />}>
        <Bundle promise={promise} />
      </Suspense>
    </div>
  );
};
