import React from 'react';
import { render, waitFor, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'
import '@testing-library/jest-dom';
import { act } from 'react-dom/test-utils';
import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import { Bundles } from './Bundles'

const testData = {
  data: {
    products: [
      {
        id: 60007741,
        image:
          "https://images.hollandandbarrettimages.co.uk/productimages/HB/320/046901_A.jpg",
        sku: "046701",
        title:
          "Holland & Barrett Pure Cod Liver Oil 1000mg 60 Capsules",
        price: "8.99",
        stock: 0,
      },
      {
        id: 6100000106,
        image:
          "https://images.hollandandbarrettimages.co.uk/productimages/HB/320/047306_A.jpg",
        sku: "047306",
        title: "Holland & Barrett Omega 3 Fish Oil 1000mg 60 Capsules",
        price: "6.99",
        stock: 10,
      },
      {
        id: 6100000246,
        image:
          "https://images.hollandandbarrettimages.co.uk/productimages/HB/320/061513_A.jpg",
        sku: "061513",
        title: "Holland & Barrett Magnesium 375mg 30 Tablets",
        price: "3.99",
        stock: 100,
      },
      {
        id: 60011168,
        image:
          "https://images.hollandandbarrettimages.co.uk/productimages/HB/320/047337_A.jpg",
        sku: "047337",
        title: "Holland & Barrett Iron & Vitamin C 14mg 30 Tablets",
        price: "2.99",
        stock: 100,
      },
    ],
  }
};

describe('Bundles', () => {
  const mock = new MockAdapter(axios);
  mock.onGet('https://hb.demo/products').reply(200, testData);

  it('Renders only products that are in stock', async () => {
    await act(() => {
      render(<Bundles />);
    });

    // TODO - use a label instead of test id (need to add markup for the label)
    const products = screen.getAllByTestId('product');

    expect(products.length).toBe(3);
    expect(screen.queryByText('Holland & Barrett Pure Cod Liver Oil 1000mg 60 Capsules')).not.toBeInTheDocument();
    expect(screen.getByText('Holland & Barrett Omega 3 Fish Oil 1000mg 60 Capsules')).toBeInTheDocument();
    expect(screen.getByText('Holland & Barrett Magnesium 375mg 30 Tablets')).toBeInTheDocument();
    expect(screen.getByText('Holland & Barrett Iron & Vitamin C 14mg 30 Tablets')).toBeInTheDocument();
  });

  it('Shows the total price of selected products', async() => {
    await act(() => {
      render(<Bundles />);
    });

    expect(screen.getByText('Total Price: £13.97')).toBeVisible();

    await userEvent.click(screen.getByTestId('sku-047306'));

    expect(screen.queryByText('Total Price: £13.97')).not.toBeInTheDocument();
    expect(screen.getByText('Total Price: £6.98')).toBeVisible();

    await userEvent.click(screen.getByTestId('sku-047306'));

    expect(screen.queryByText('Total Price: £6.98')).not.toBeInTheDocument();
    expect(screen.getByText('Total Price: £13.97')).toBeVisible();
  });
})