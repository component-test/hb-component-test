import React from 'react';
import styled from '@emotion/styled'

const ProductCard = styled.div({
  'background-color': '#fff',
});

const ProductImage = styled.img({
  height: '100px'
})

const Button = styled.button({
  'background-color': 'green',
})

export const Product = ({
  product: {
    id,
    image,
    sku,
    title,
    price,
    stock
  },
  handleCheckboxUpdate,
  isSelected
}) => {
  return (
    <ProductCard data-testid="product">
      <ProductImage src={`${image}`} alt="" />
      <input type="checkbox" checked={isSelected} onChange={handleCheckboxUpdate} data-testid={`sku-${sku}`}/>
      <div>{title}</div>
      <div>£{price}</div>
    </ProductCard>
  )
}